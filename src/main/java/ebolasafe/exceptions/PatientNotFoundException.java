package ebolasafe.exceptions;

public class PatientNotFoundException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PatientNotFoundException(Long id) {
		super(String.format("Patient not avaiable! (Patient id: %d)", id));
	}

	public PatientNotFoundException() {
		// TODO Auto-generated constructor stub
	}
}
