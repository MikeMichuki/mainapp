package ebolasafe.exceptions;

public class TemperatureReadingNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TemperatureReadingNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TemperatureReadingNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
