package ebolasafe.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class TemperatureReading {
	@Id
	@GeneratedValue
	Long id;
	Date dateOfInput;
	Double degrees;
	double day;
	boolean deleted;
}
