package ebolasafe.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class SymptomsReport {
	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	Question question;
	
	String Answer;
	boolean deleted;
}
