package ebolasafe.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Person {
	 @Id
	 @GeneratedValue
	 Long id;
	 String name;
	 String phoneNumber;
	 String email;
	 
	 @Temporal(TemporalType.DATE)
	 Date registrationDate; 
	 boolean deleted;
}
