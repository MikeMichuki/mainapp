package ebolasafe.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class Patient extends Person{

	String ModeOfContact;
	String Relationship;
	
	@Temporal(TemporalType.DATE)
	Date DateOfContact;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<NextOfKin> nextofKins;
}
