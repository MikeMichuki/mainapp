package ebolasafe.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;


@Entity
@Data
public class Notification {
	@Id
	@GeneratedValue
	Long id;
	String title;
	String message;
	Date created;
	ReadStatus status;
	boolean deleted;
	
	public enum ReadStatus {
		READ, UNREAD
	}
}


