package ebolasafe.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Question {
	@Id
	@GeneratedValue
	Long id;
	
	String question;
	double position;
	Date creationDate;
	QuestionType QuestionType;
	
	public enum QuestionType {
		YESNO, TEXTAREA,NUMBER,DATE
	}
	boolean deleted;
}

