package ebolasafe.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "authorities")
public class Authority {
	@Id
	@GeneratedValue
	Long id;
	
	@ManyToOne
	@JoinColumn(referencedColumnName = "username", name = "username")
	User username;
	String authority;
}