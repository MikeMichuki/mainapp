package ebolasafe.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User {
	@Id
	String username;
	String password;
	boolean enabled;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<NextOfKin> nextOfkin;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<Notification> notifications;
	
	public enum VerifyStatus {
		APPROVED, PENDING
	}
}

