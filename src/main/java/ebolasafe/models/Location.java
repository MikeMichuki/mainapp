package ebolasafe.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Location {
	@Id
	@GeneratedValue
	Long id;
	float Longitude;
	float latitude;
	float altitude;
	String country;
	String town;
	
	@ManyToMany
	List<Patient> patients;
	
	@OneToMany
	List<Report> reports;
}
