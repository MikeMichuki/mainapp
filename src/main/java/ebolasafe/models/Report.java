package ebolasafe.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class Report {
	@Id
	@GeneratedValue
	Long id;
	Date createdDate;
	double AverageTemp;
	
	@OneToOne
	Location location;
	
	@OneToOne
	Patient patient;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<SymptomsReport> symptomsReports;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<TemperatureReading> tempReadings;
	boolean deleted;
}
