package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.TemperatureReadingResource;
import ebolasafe.models.TemperatureReading;
import ebolasafe.restControllers.TemperatureReadingController;

@Service
public class TemperatureReadingAssembler extends ResourceAssemblerSupport <TemperatureReading,TemperatureReadingResource>{

	public TemperatureReadingAssembler() {
		super(TemperatureReadingController.class, TemperatureReadingResource.class);
	}

	@Override
	public TemperatureReadingResource toResource(TemperatureReading tempreading) {
		TemperatureReadingResource tempRes = new TemperatureReadingResource();
		tempRes.setIdRes(tempreading.getId());
		tempRes.setDateOfInput(tempreading.getDateOfInput());
		tempRes.setDay(tempreading.getDay());
		tempRes.setDegrees(tempreading.getDegrees());
		return tempRes;
		
	}
	
	public List<TemperatureReadingResource> toResources(List<TemperatureReading> all) {
		List<TemperatureReadingResource> tempres = new ArrayList<TemperatureReadingResource>();
		for (TemperatureReading tempreading : all) {
			tempres.add(toResource(tempreading));
		}
		return tempres;
	}
}
