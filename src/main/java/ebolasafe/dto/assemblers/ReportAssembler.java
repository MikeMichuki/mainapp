package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.ReportResource;
import ebolasafe.models.Report;
import ebolasafe.restControllers.ReportsController;

@Service
public class ReportAssembler extends ResourceAssemblerSupport<Report, ReportResource>{

	public ReportAssembler() {
		super(ReportsController.class, ReportResource.class);
	}
	@Autowired
	private SymptomsReportsAssembler symptomsassembler;
	
	@Autowired
	private TemperatureReadingAssembler tempassembler;
	
	@Override
	public ReportResource toResource(Report report) {
		ReportResource reportRes = new ReportResource();
		reportRes.setIdRes(report.getId());
		reportRes.setAverageTemp(report.getAverageTemp());
		reportRes.setCreatedDate(report.getCreatedDate());
		reportRes.setSymptresources(symptomsassembler.toResources(report.getSymptomsReports()));
		reportRes.setTempResources(tempassembler.toResources(report.getTempReadings()));
		return reportRes;
	}
	public List<ReportResource> toResources(List<Report> all) {
		List<ReportResource> reportRes = new ArrayList<ReportResource>();
		for (Report report : all) {
			reportRes.add(toResource(report));
		}
		return reportRes;
	}
}
