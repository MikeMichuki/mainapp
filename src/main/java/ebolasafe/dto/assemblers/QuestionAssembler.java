package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.QuestionResource;
import ebolasafe.models.Question;
import ebolasafe.restControllers.QuestionsController;

@Service
public class QuestionAssembler extends ResourceAssemblerSupport<Question, QuestionResource> {

	public QuestionAssembler() {
		super(QuestionsController.class, QuestionResource.class);
	}

	@Override
	public QuestionResource toResource(Question question) {
		QuestionResource questionResource = new QuestionResource();
		questionResource.setIdRes(question.getId());
		questionResource.setCreationDate(question.getCreationDate());
		questionResource.setPosition(question.getPosition());
		questionResource.setQuestion(question.getQuestion());
		questionResource.setQuestionType(question.getQuestionType());
		
		return questionResource;
	}
		
	public List<QuestionResource> toResources(List<Question> all) {
		List<QuestionResource> questionsRes = new ArrayList<QuestionResource>();
		for (Question question : all) {
			questionsRes.add(toResource(question));
		}
		return questionsRes;
	}
}
