package ebolasafe.dto.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.SymptomsResource;
import ebolasafe.models.SymptomsReport;
import ebolasafe.restControllers.SymptomsReportController;

@Service
public class SymptomsReportsAssembler extends ResourceAssemblerSupport<SymptomsReport, SymptomsResource> {

	public SymptomsReportsAssembler() {
		super(SymptomsReportController.class, SymptomsResource.class);
	}
	private static final QuestionAssembler questionassembler = new QuestionAssembler();
	
	@Override
	public SymptomsResource toResource(SymptomsReport symptoms) {
		SymptomsResource symptRes = new SymptomsResource();
		symptRes.setIdRes(symptoms.getId());
		symptRes.setAnswer(symptoms.getAnswer());
		symptRes.setQuestion(questionassembler.toResource(symptoms.getQuestion()));
		return symptRes;
	}
}
