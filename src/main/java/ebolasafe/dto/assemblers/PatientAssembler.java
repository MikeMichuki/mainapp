package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.PatientResource;
import ebolasafe.models.Patient;
import ebolasafe.restControllers.PatientsController;

@Service
public class PatientAssembler extends ResourceAssemblerSupport<Patient, PatientResource> {
	public PatientAssembler() {
		super(PatientsController.class, PatientResource.class);
	}
	
	@Autowired
	private NextOfKinAssembler nextofkinassembler;

	@Override
	public PatientResource toResource(Patient patient) {
		PatientResource patientRes = new PatientResource();
		patientRes.setIdRes(patient.getId());
		patientRes.setEmail(patient.getEmail());
		patientRes.setName(patient.getName());
		patientRes.setRegistrationDate(patient.getRegistrationDate());
		patientRes.setDateOfContact(patient.getDateOfContact());
		patientRes.setPhoneNumber(patient.getPhoneNumber());
		patientRes.setRelationship(patient.getRelationship());
		patientRes.setNextofKins(nextofkinassembler.toResources(patient.getNextofKins()));
		return patientRes;
	}
	
	public List<PatientResource> toResources(List<Patient> all) {
		List<PatientResource> patientsRes = new ArrayList<PatientResource>();
		for (Patient patient : all) {
			patientsRes.add(toResource(patient));
		}
		return patientsRes;
	}
}
