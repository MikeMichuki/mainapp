package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.NextOfKinResource;
import ebolasafe.models.NextOfKin;
import ebolasafe.restControllers.PatientsController;

@Service
public class NextOfKinAssembler extends ResourceAssemblerSupport<NextOfKin,NextOfKinResource>{

	public NextOfKinAssembler() {
		super(PatientsController.class, NextOfKinResource.class);
	}

	@Override
	public NextOfKinResource toResource(NextOfKin nextOfKin) {
		NextOfKinResource nextOfkinRes = new NextOfKinResource();
		nextOfkinRes.setIdRes(nextOfKin.getId());
		nextOfkinRes.setName(nextOfKin.getName());
		nextOfkinRes.setEmail(nextOfKin.getEmail());
		nextOfkinRes.setPhoneNumber(nextOfKin.getPhoneNumber());
		return nextOfkinRes;
	}
	
	public List<NextOfKinResource> toResources(List<NextOfKin> all) {
		List<NextOfKinResource> nextOfKinRes = new ArrayList<NextOfKinResource>();
		for (NextOfKin nextOfKin : all) {
			nextOfKinRes.add(toResource(nextOfKin));
		}
		return nextOfKinRes;
	}
}
