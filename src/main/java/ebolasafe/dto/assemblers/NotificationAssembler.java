package ebolasafe.dto.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.NotificationResource;
import ebolasafe.models.Notification;
import ebolasafe.restControllers.NotificationsController;

@Service
public class NotificationAssembler extends ResourceAssemblerSupport<Notification, NotificationResource> {

	public NotificationAssembler() {
		super(NotificationsController.class, NotificationResource.class);
	}

	@Override
	public NotificationResource toResource(Notification notification) {
		NotificationResource notificationresource = new NotificationResource();
		notificationresource.setIdRes(notification.getId());
		notificationresource.setCreated(notification.getCreated());
		notificationresource.setMessage(notification.getMessage());
		notificationresource.setStatus(notification.getStatus());
		notificationresource.setTitle(notification.getTitle());
		return notificationresource;
	}
	
	public List<NotificationResource> toResources(List<Notification> all) {
		List<NotificationResource> newNotifications = new ArrayList<NotificationResource>();
		for (Notification notification : all) {
			newNotifications.add(toResource(notification));
		}
		return newNotifications;
	}
}
