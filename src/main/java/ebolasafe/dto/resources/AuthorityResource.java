package ebolasafe.dto.resources;

import javax.xml.bind.annotation.XmlRootElement;

import ebolasafe.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "authority")
public class AuthorityResource extends ResourceSupport{
	Long resId;
    String username;
    String authority;
}