package ebolasafe.dto.resources;

import javax.xml.bind.annotation.XmlRootElement;

import ebolasafe.models.User.VerifyStatus;
import ebolasafe.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "user")
public class UserRegisterResource extends ResourceSupport{
    String username;
    String email;
    String password;
    VerifyStatus verifystatus;
}