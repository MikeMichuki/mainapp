package ebolasafe.dto.resources;

import javax.xml.bind.annotation.XmlRootElement;

import ebolasafe.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "user")
public class UserResource  extends ResourceSupport{
    String username;
    boolean enabled;
}
