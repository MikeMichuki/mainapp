package ebolasafe.dto.resources;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ebolasafe.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class PatientResource extends ResourceSupport{
	Long idRes;
	String name;
	String phoneNumber;
	String email;
	
	@DateTimeFormat(iso=ISO.DATE)
	Date registrationDate;
	
	@DateTimeFormat(iso=ISO.DATE)
	Date DateOfContact;
	
	String ModeOfContact;
	String Relationship;
	List<NextOfKinResource> nextofKins;
}
