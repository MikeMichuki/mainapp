package ebolasafe.dto.resources;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ebolasafe.models.Question.QuestionType;
import ebolasafe.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class QuestionResource extends ResourceSupport{
	
	Long idRes;
	String question;
	double position;
	QuestionType QuestionType;
	
	@DateTimeFormat(iso=ISO.DATE)
	Date creationDate;
}
