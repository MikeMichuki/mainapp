package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.TemperatureReadingAssembler;
import ebolasafe.dto.resources.TemperatureReadingResource;
import ebolasafe.exceptions.TemperatureReadingNotFoundException;
import ebolasafe.models.TemperatureReading;
import ebolasafe.repositories.TemperatureReadingRepository;
import ebolasafe.services.TemperatureReadingsManager;


@RestController
@RequestMapping("/rest/temperature")
public class TemperatureReadingController {
TemperatureReadingAssembler assembler = new TemperatureReadingAssembler();
	
	@Autowired
	TemperatureReadingsManager manager;
	
	@Autowired
	TemperatureReadingRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<TemperatureReadingResource> getAllTemperatureReadings()
			throws Exception {
		List<TemperatureReading> TemperatureReadings = manager.getAllTemperatureReadings();
		return assembler.toResources(TemperatureReadings);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public TemperatureReadingResource createTemperatureReading(
			@RequestBody TemperatureReadingResource _TemperatureReading) throws Exception {
		TemperatureReading TemperatureReading = manager.createTemperatureReading(_TemperatureReading);
		return assembler.toResource(TemperatureReading);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public TemperatureReadingResource getTemperatureReading(
			@PathVariable("id") Long id) throws Exception {
		TemperatureReading TemperatureReading = manager.getTemperatureReading(id);
		if (TemperatureReading == null)
			throw new TemperatureReadingNotFoundException();
		return assembler.toResource(TemperatureReading);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public TemperatureReadingResource updateTemperatureReading(@PathVariable("id") Long id,
			@RequestBody TemperatureReadingResource _TemperatureReading) {
		TemperatureReading TemperatureReading = manager.updateTemperatureReading(id,_TemperatureReading);
		return assembler.toResource(TemperatureReading);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public TemperatureReadingResource deleteTemperatureReading(@PathVariable("id") Long id) {
		TemperatureReading TemperatureReading = manager.deleteTemperatureReading(id);
		return assembler.toResource(TemperatureReading);
	}
}
