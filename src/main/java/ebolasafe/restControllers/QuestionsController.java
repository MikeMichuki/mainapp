package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.QuestionAssembler;
import ebolasafe.dto.resources.QuestionResource;
import ebolasafe.exceptions.QuestionNotFoundException;
import ebolasafe.models.Question;
import ebolasafe.repositories.QuestionsRepository;
import ebolasafe.services.QuestionManager;

@RestController
@RequestMapping("/rest/questions")
public class QuestionsController {
QuestionAssembler assembler = new QuestionAssembler();
	
	@Autowired
	QuestionManager manager;
	
	@Autowired
	QuestionsRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<QuestionResource> getAllQuestions()
			throws Exception {
		List<Question> Questions = manager.getAllQuestions();
		return assembler.toResources(Questions);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public QuestionResource createQuestion(
			@RequestBody QuestionResource _Question) throws Exception {
		Question Question = manager.createQuestion(_Question);
		return assembler.toResource(Question);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public QuestionResource getQuestion(
			@PathVariable("id") Long id) throws Exception {
		Question Question = manager.getQuestion(id);
		if (Question == null)
			throw new QuestionNotFoundException();
		return assembler.toResource(Question);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public QuestionResource updateQuestion(@PathVariable("id") Long id,
			@RequestBody QuestionResource _Question) {
		Question Question = manager.updateQuestion(id,_Question);
		return assembler.toResource(Question);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public QuestionResource deleteQuestion(@PathVariable("id") Long id) {
		Question Question = manager.deleteQuestion(id);
		return assembler.toResource(Question);
	}
}
