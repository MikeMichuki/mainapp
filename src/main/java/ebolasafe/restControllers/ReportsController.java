package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.ReportAssembler;
import ebolasafe.dto.resources.ReportResource;
import ebolasafe.exceptions.ReportNotFoundException;
import ebolasafe.models.Report;
import ebolasafe.repositories.ReportsRepository;
import ebolasafe.services.ReportManager;


@RestController
@RequestMapping("/rest/reports")
public class ReportsController {
	ReportAssembler assembler = new ReportAssembler();
	
	@Autowired
	ReportManager manager;
	
	@Autowired
	ReportsRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<ReportResource> getAllReports()
			throws Exception {
		List<Report> reports = manager.getAllReports();
		return assembler.toResources(reports);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public ReportResource createReport(
			@RequestBody ReportResource _Report) throws Exception {
		Report report = manager.createReport(_Report);
		return assembler.toResource(report);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ReportResource getReport(
			@PathVariable("id") Long id) throws Exception {
		Report report = manager.getReport(id);
		if (report == null)
			throw new ReportNotFoundException();
		return assembler.toResource(report);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ReportResource updateReport(@PathVariable("id") Long id,
			@RequestBody ReportResource _Report) {
		Report report = manager.updateReport(id,_Report);
		return assembler.toResource(report);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ReportResource deleteReport(@PathVariable("id") Long id) {
		Report report = manager.deleteReport(id);
		return assembler.toResource(report);
	}
}
