package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.SymptomsReportsAssembler;
import ebolasafe.dto.resources.SymptomsResource;
import ebolasafe.exceptions.SymptomsReportNotFoundException;
import ebolasafe.models.SymptomsReport;
import ebolasafe.repositories.SymptomsRepository;
import ebolasafe.services.SymptomsReportsManager;


@RestController
@RequestMapping("/rest/symptomsSymptomsReport")
public class SymptomsReportController {
	SymptomsReportsAssembler assembler = new SymptomsReportsAssembler();
	
	@Autowired
	SymptomsReportsManager manager;
	
	@Autowired
	SymptomsRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<SymptomsResource> getAllSymptomsReports()
			throws Exception {
		List<SymptomsReport> SymptomsReports = manager.getAllSymptomsReports();
		return assembler.toResources(SymptomsReports);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public SymptomsResource createSymptomsReport(
			@RequestBody SymptomsResource _SymptomsReport) throws Exception {
		SymptomsReport SymptomsReport = manager.createSymptomsReport(_SymptomsReport);
		return assembler.toResource(SymptomsReport);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public SymptomsResource getSymptomsReport(
			@PathVariable("id") Long id) throws Exception {
		SymptomsReport SymptomsReport = manager.getSymptomsReport(id);
		if (SymptomsReport == null)
			throw new SymptomsReportNotFoundException();
		return assembler.toResource(SymptomsReport);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public SymptomsResource updateSymptomsReport(@PathVariable("id") Long id,
			@RequestBody SymptomsResource _SymptomsReport) {
		SymptomsReport SymptomsReport = manager.updateSymptomsReport(id,_SymptomsReport);
		return assembler.toResource(SymptomsReport);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public SymptomsResource deleteSymptomsReport(@PathVariable("id") Long id) {
		SymptomsReport SymptomsReport = manager.deleteSymptomsReport(id);
		return assembler.toResource(SymptomsReport);
	}
}
