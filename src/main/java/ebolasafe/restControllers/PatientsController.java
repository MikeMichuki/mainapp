package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.PatientAssembler;
import ebolasafe.dto.resources.PatientResource;
import ebolasafe.exceptions.PatientNotFoundException;
import ebolasafe.models.Patient;
import ebolasafe.repositories.PatientsRepository;
import ebolasafe.services.PatientsManager;

@RestController
@RequestMapping("/rest/patients")
public class PatientsController {
	PatientAssembler assembler = new PatientAssembler();
	
	@Autowired
	PatientsManager manager;
	
	@Autowired
	PatientsRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<PatientResource> getAllPatients()
			throws Exception {
		List<Patient> patients = manager.getAllPatients();
		return assembler.toResources(patients);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public PatientResource createPatient(
			@RequestBody PatientResource _patient) throws Exception {
		Patient patient = manager.createPatient(_patient);
		return assembler.toResource(patient);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public PatientResource getPatient(
			@PathVariable("id") Long id) throws Exception {
		Patient patient = manager.getPatient(id);
		if (patient == null)
			throw new PatientNotFoundException();
		return assembler.toResource(patient);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public PatientResource updatePatient(@PathVariable("id") Long id,
			@RequestBody PatientResource _patient) {
		Patient patient = manager.updatePatient(id,_patient);
		return assembler.toResource(patient);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public PatientResource deletePatient(@PathVariable("id") Long id) {
		Patient patient = manager.deletePatient(id);
		return assembler.toResource(patient);
	}
}
