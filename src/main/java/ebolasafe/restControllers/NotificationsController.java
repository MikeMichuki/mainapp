package ebolasafe.restControllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ebolasafe.dto.assemblers.NotificationAssembler;
import ebolasafe.dto.resources.NotificationResource;
import ebolasafe.exceptions.NotificationNotFoundException;
import ebolasafe.models.Notification;
import ebolasafe.repositories.NotificationsRepository;
import ebolasafe.services.NotificationsManager;

@RestController
@RequestMapping("/rest/notifications")
public class NotificationsController {
	NotificationAssembler assembler = new NotificationAssembler();
	
	@Autowired
	NotificationsManager manager;
	
	@Autowired
	NotificationsRepository repo;
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<NotificationResource> getAllNotifications()
			throws Exception {
		List<Notification> Notifications = manager.getAllNotifications();
		return assembler.toResources(Notifications);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ResponseStatus(HttpStatus.CREATED)
	public NotificationResource createNotification(
			@RequestBody NotificationResource _Notification) throws Exception {
		Notification Notification = manager.createNotification(_Notification);
		return assembler.toResource(Notification);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public NotificationResource getNotification(
			@PathVariable("id") Long id) throws Exception {
		Notification Notification = manager.getNotification(id);
		if (Notification == null)
			throw new NotificationNotFoundException();
		return assembler.toResource(Notification);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public NotificationResource updateNotification(@PathVariable("id") Long id,
			@RequestBody NotificationResource _Notification) {
		Notification Notification = manager.updateNotification(id,_Notification);
		return assembler.toResource(Notification);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public NotificationResource deleteNotification(@PathVariable("id") Long id) {
		Notification Notification = manager.deleteNotification(id);
		return assembler.toResource(Notification);
	}
}
