package ebolasafe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.AuthorityResource;
import ebolasafe.dto.resources.UserRegisterResource;
import ebolasafe.models.Authority;
import ebolasafe.models.User;
import ebolasafe.repositories.AuthorityRepository;
import ebolasafe.repositories.UsersRepository;


@Service
public class UserService {

	@Autowired
	UsersRepository repo;

	@Autowired
	AuthorityRepository authoRepo;

	public List<User> getAll() {
		return repo.findAll();
	}

	public User getUser(String id) {
		return repo.findOne(id);
	}

	public User createUser(UserRegisterResource userRes) {
		User user = new User();
		user.setUsername(userRes.getUsername());
		user.setPassword(userRes.getPassword());
		user.setEnabled(true);

		user = repo.saveAndFlush(user);

		Authority authority = new Authority();
		authority.setUsername(user);
		authority.setAuthority("ROLE_ADMIN");
		authoRepo.saveAndFlush(authority);

		return user;
	}

	public Authority addUserAuthority(String username, AuthorityResource authorityRes) {
		Authority authority = new Authority();
		authority.setUsername(getUser(username));
		authority.setAuthority(authorityRes.getAuthority());
		authority = authoRepo.saveAndFlush(authority);
		return authority;
	}

	public List<Authority> getUserAuthorities(String username) {
		return authoRepo.findByUsername(username);
	}

	public void deleteUserAuthority(String username, Long id) {
		authoRepo.delete(id);
	}
}
