package ebolasafe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ebolasafe.dto.resources.PatientResource;
import ebolasafe.models.Patient;
import ebolasafe.repositories.PatientsRepository;

@Service
public class PatientsManager {

	@Autowired
	PatientsRepository patientsRepository;
	
	public List<Patient> getAllPatients() {
		return patientsRepository.findAll();
	}

	public Patient getPatient(Long id) {
		return patientsRepository.findOne(id);
	}

	public Patient deletePatient(Long id) {
		patientsRepository.delete(id);
		return null;
	}

	public Patient updatePatient(Long id, PatientResource patientRes) {		
		Patient patient = patientsRepository.findOne(id);
		patient.setName(patientRes.getName());
		patient.setEmail(patientRes.getEmail());
		patient.setDateOfContact(patientRes.getDateOfContact());
		patient.setModeOfContact(patientRes.getModeOfContact());
		patient.setPhoneNumber(patientRes.getPhoneNumber());
		patient.setRelationship(patientRes.getRelationship());
		return patient;
	}

	public Patient createPatient(PatientResource _patient) {
		Patient patient = new Patient();
		patient.setName(_patient.getName());
		patient.setEmail(_patient.getEmail());
		patient.setDateOfContact(_patient.getDateOfContact());
		patient.setModeOfContact(_patient.getModeOfContact());
		patient.setPhoneNumber(_patient.getPhoneNumber());
		patient.setRelationship(_patient.getRelationship());
		return patient;
	}

}
