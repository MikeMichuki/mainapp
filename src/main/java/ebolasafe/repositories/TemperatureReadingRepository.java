package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.TemperatureReading;

@Repository
public interface TemperatureReadingRepository extends JpaRepository <TemperatureReading, Long>{

}
