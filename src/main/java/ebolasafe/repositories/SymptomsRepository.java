package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.SymptomsReport;

@Repository
public interface SymptomsRepository extends JpaRepository<SymptomsReport, Long>{

}
