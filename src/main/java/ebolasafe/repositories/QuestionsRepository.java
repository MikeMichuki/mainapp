package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.Question;

@Repository
public interface QuestionsRepository extends JpaRepository <Question, Long>{

}
