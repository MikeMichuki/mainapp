package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.Notification;

@Repository
public interface NotificationsRepository extends JpaRepository <Notification,Long>{

}
