package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.Report;

@Repository
public interface ReportsRepository extends JpaRepository<Report, Long>{

}
