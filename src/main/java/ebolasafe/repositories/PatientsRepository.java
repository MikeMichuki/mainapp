package ebolasafe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ebolasafe.models.Patient;

@Repository
public interface PatientsRepository extends JpaRepository<Patient,Long>{

}
