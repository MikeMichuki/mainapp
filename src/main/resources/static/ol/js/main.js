var app = angular.module('project', ["ngRoute", "ngCookies"]);

app.config(function($routeProvider,$locationProvider, $httpProvider) {
	$routeProvider
		.when('/login', {
	      controller:'LoginController',
	      templateUrl:'views/login.html'
	    })
	    .when('/plants', {
	    	controller:'PlantsController',
	      	templateUrl:'views/plants/list.html',
			      resolve: {
			    	  permission: function(RoleBasedAccessService, $route) {
			              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
			      			}
			      		}
	    })
	    .when('/plants/create', {
	    	controller: 'CreatePlantController',
	    	templateUrl: 'views/plants/create.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    
	    })
	    .when('/plants/update', {
	    	controller: 'UpdatePlantController',
	    	templateUrl: 'views/plants/update.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    
	    })
	    .when('/plants/show', {
	    	controller:'PlantShowController',
	      	templateUrl:'views/plants/show.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    
	    })
	    .when('/plants/search', {
	    	controller:'SearchPlantController',
	      	templateUrl:'views/plants/search.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    
	    })
	    .when('/invoices', {
	    	controller: 'InvoiceController',
          	templateUrl: 'views/invs/list.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    
	    })
	    .when('/pos', {
	    	controller:'POSController',
	      	templateUrl:'views/pos/list.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    })
	    .when('/pos/show', {
	    	controller:'POSShowController',
	      	templateUrl:'views/pos/show.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    })
	    .when('/customers', {
	    	controller: 'CustomersController',
	    	templateUrl: 'views/customers/list.html',
		      resolve: {
		    	  permission: function(RoleBasedAccessService, $route) {
		              return RoleBasedAccessService.permissionCheck(["ROLE_SENG","ROLE_WENG","ROLE_ADMIN"]);
		      			}
		      		}	    })
	    .when('/logout', {
        	redirectTo:'/login'
        })
	    .otherwise({
		      redirectTo:'/login'
		});
	
	$httpProvider.interceptors.push(function ($q, $rootScope, $location, $window) {
        return {
        	'request': function(config) {
        		var isRestCall = config.url.indexOf('/rest') == 0;
        		if (isRestCall) {
        			config.headers['Authorization'] = 'Basic ' + $rootScope.code;
        		}
        		return config || $q.when(config);
        	},
        	'responseError': function(rejection) {
        		var status = rejection.status;
        		var config = rejection.config;
        		var method = config.method;
        		var url = config.url;
      
        		if (status == 401) {
//        			$rootScope.logout();
        			$window.history.back();
        		} else {
        			$rootScope.error = method + " on " + url + " failed with status " + status;
        		}
              
        		return $q.reject(rejection);
				}
			};
		}
	);
});
	
app.run(function($rootScope, $location, $cookieStore) {
    var user = $cookieStore.get("user");
    var code = $cookieStore.get("code");
    if (user !== undefined && code !== undefined) {
        $rootScope.user = user;
        $rootScope.code = code;
    }

    $rootScope.logout = function() {
        delete $rootScope.user;
        delete $rootScope.code;

        $cookieStore.remove("user");
        $cookieStore.remove("code");
        $cookieStore.remove("userRoles");
        $location.path("/login");
    };
});

app.service("RoleBasedAccessService", function($rootScope, $location, $q,
        $cookieStore) {
    return {
        permissionCheck : function(roleCollection) {
            var deferred = $q.defer();
            var userRoles = $cookieStore.get("userRoles");

            console.log("-------------");
            console.log(roleCollection);
            console.log(userRoles);
            console.log("-------------");

            var matchingRoles = userRoles.filter(function(role) {
                return roleCollection.indexOf(role) != -1;
            });
            if (userRoles !== undefined && matchingRoles.length > 0)
                deferred.resolve();
            else {
                $location.path("/login");
                $rootScope.$on("$locationChangeSuccess",
                        function(next, current) {
                            deferred.resolve();
                        });
            }
            return deferred.promise;
        }
    };
});