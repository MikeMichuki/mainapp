var app = angular.module('project');

app.controller("LoginController", function($scope, $http, $location, $rootScope, $cookieStore) {
	$scope.username = "";
	$scope.password = "";
	delete $rootScope.user;
	delete $rootScope.code;
	$cookieStore.remove("user");
	$cookieStore.remove("code");				
	$cookieStore.remove("userRoles");

	$scope.login = function () {
		var code = window.btoa($scope.username + ":" + $scope.password);
		console.log(code);
		$rootScope.user = $scope.username;
		$rootScope.code = code;
		$http.post("/rest/authentication")
			.success(function(data, status, headers, config) {
				console.log(data.roles);
				$cookieStore.put("userRoles", data.roles);
				$cookieStore.put("user", $scope.username);
				$cookieStore.put("code", code);				
    			$location.path( "/plants" );
			});
	};
});

//Plants
app.controller('PlantsController', function($scope, $http) {
	$scope.plants = [];
	$http.get('/rest/plants').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.plants = data;
	});
	
});

app.controller('CreatePlantController', function($scope, $http, $location) {
	$scope.setPlant = function () {
		plant = { name: $scope.name, description: $scope.description, price: $scope.price};
		console.log($scope.name);
		$http.post('rest/plants', plant).success(function(data, status, headers, config) {
            console.log($scope);
            $location.path('/plants');
        });
    };
});

app.controller('SearchPlantController', function($scope, $http, $location) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    
    $scope.catalogShown = false;

    $scope.execQuery = function () {
        $http.get('/rest/plants', {params: {name: $scope.name, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
            $scope.plants = data;
            console.log(data);
            $scope.catalogShown = true;
        });
    };
});

app.controller('UpdatePlantController', function($scope, $http, $location) {
    $scope.plants = [];
    $scope.phr;
    $scope.phrUrl = '';

	$http.get($scope.phrurl, {params: { plant: $scope.name , startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
	    $scope.phr = data;
	    $scope.plant = data.plant;
	    $scope.name = data.plant.description;
	    $scope.startDate = new Date(data.startDate);
	    $scope.endDate =new Date(data.endDate);
    });

    $scope.catalogShown = false;
    
    $scope.execQuery = function () {
        $http.get('/rest/plants', {params: {plant: $scope.name, startDate: $scope.name, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
            $scope.plants = data;
            $scope.catalogShown = true;
        });
    };
    
    $scope.updatePHR = function () {
    	phr = { startDate: $scope.startDate, endDate: $scope.endDate};
    	$http.post($scope.phrurl, phr).success(function(data, status, headers, config) {
            $location.path('/phrs');
        });
    };
    
    $scope.setPlant = function (selectedPlant) {
        phr = { plant: selectedPlant, startDate: $scope.startDate, endDate: $scope.endDate};
        window.alert("About to set" + $scope.phrurl);
        var diffDays = Math.floor(($scope.endDate.getTime() - $scope.startDate.getTime())/86400000); 
        if(diffDays >= 0) {
            $http.post($scope.phrurl, phr).success(function(data, status, headers, config) {
                $location.path('/phrs');
            });
        }else{
        	window.alert("Your End Date must be Greater than start Date");
        }
    };
});

////////////////////////////////////////////////


app.controller('InvoiceController', function($rootScope,$scope, $http, $route,$location) {
    
	$scope.invs = [];
	$scope.poUrlSelf = '';
    $http.get('/rest/invs').success(function(data, status, headers, config) {
        console.log(data);
        $scope.invs = data;
    });
    
    $scope.viewpo = function(poref) {
    	 console.log(poref);
    	 $rootScope.poUrlSelf = poref;
    	 $http.post(poref).success(function(data, status, headers, config) {
     		console.log(data);
     		$location.path('/pos/show');
    	 });
    };
    
    $scope.follow = function(link) {
        console.log(link);
        console.log(link.method);
        if (link.method == 'POST') {
        	$http.post(link.href).success(function(data, status, headers, config) {
            		console.log(data);
            		$route.reload();});
        } else if (link.method == 'DELETE') {
        	$http.delete(link.href).success(function(data, status, headers, config) {
            		console.log(data);
                	$route.reload();});
        }
    };
});

app.controller('POSController', function($rootScope,$scope, $http, $route,$location) {
    
	$scope.pos = [];
	$scope.poUrlSelf = '';
    $http.get('/rest/pos/local').success(function(data, status, headers, config) {
    	console.log(JSON.stringify(data));
        $scope.pos = data;
    });
    
    $scope.viewplant = function(plantref) {
   	 console.log(plantref);
   	 $rootScope.planturlSelf = plantref;
   	 $http.post(plantref).success(function(data, status, headers, config) {
    		console.log(data);
    		$location.path('/plants/show');
   	 });
   };
    
    $scope.follow = function(link) {
        console.log(link);
        console.log(link.method);
        if (link.method == 'POST') {
        	$http.post(link.href).success(function(data, status, headers, config) {
            		console.log(data);
            		$route.reload();});
        } else if (link.method == 'DELETE') {
        	if (link.rel == 'updatePHR') {
            	console.log($route);
            	$rootScope.pourl = link.href;
            	$location.path('/phrs/update');
            }else {
            		$http.delete(link.href).success(function(data, status, headers, config) {
            		console.log(data);
                	$route.reload();}); 
            }
        }
    };
});

app.controller('POSShowController', function($rootScope,$scope, $http, $route,$location) {
    $scope.pos = [];
	$http.get($scope.poUrlSelf).success(function(data, status, headers, config) {
        console.log($scope.poUrlSelf);
        console.log($scope.poUrlSelf);
        $scope.pos = data;
    });
	
});


app.controller('PlantShowController', function($rootScope,$scope, $http, $route,$location) {
    $scope.plant = [];
	$http.get($scope.planturlSelf).success(function(data, status, headers, config) {
        console.log($scope.planturlSelf);
        $scope.plant = data;
    });
});

app.controller('CustomersController', function($scope, $http) {
	$scope.customers = [];
	$http.get('/rest/customers').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.customers = data;
	});
	
});
